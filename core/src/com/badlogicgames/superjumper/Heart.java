package com.badlogicgames.superjumper;

/**
 * Created by Oriol on 17/04/2017.
 */

public class Heart extends GameObject {
    public static final float HEART_WIDTH = 1;
    public static final float HEART_HEIGHT = 1;

    public Heart(float x, float y) {
        super(x, y, HEART_WIDTH, HEART_HEIGHT);
    }
}

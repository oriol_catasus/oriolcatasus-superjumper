package com.badlogicgames.superjumper;

/**
 * Created by Oriol on 14/04/2017.
 */

public class Jetpack extends GameObject {

    public static final float JETPACK_HEIGHT = 1f;
    public static final float JETPACK_WIDTH = 1f;

    public float stateTime;

    public Jetpack(float x, float y) {
        super(x, y, JETPACK_WIDTH, JETPACK_HEIGHT);
        stateTime = 0;

        //Particle
        Assets.spawnParticle(Assets.jetpackParticlePool, x - 0.4f, y - 0.5f);
    }

    public void update(float deltaTime) {
        stateTime += deltaTime;
    }
}

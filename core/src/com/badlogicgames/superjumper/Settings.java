/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.badlogicgames.superjumper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Settings {
	public static boolean soundEnabled = true;
	public final static int MAX_NUM_RECORDS_TO_SHOW = 12;
	//public static int[] highscores;
	//public static String[] highscoresNames;
	public static TreeMap<Integer, String> highscoresMap = new TreeMap<Integer, String>(Collections.reverseOrder());
	public final static String file = ".superjumper";

	public final static String URL = "https://oriolcatasus-records.herokuapp.com";
	public final static String URL_POST = "/record";
	public final static String URL_GET = "/record/app/jump";

	public static String playerName = "Default";

	public static boolean internetError = false;

	public static void load () {
		try {
			FileHandle filehandle = Gdx.files.external(file);
			
			String[] strings = filehandle.readString().split("\n");
			
			soundEnabled = Boolean.parseBoolean(strings[0]);
			/*for (int i = 0; i < 5; i++) {
				highscores[i] = Integer.parseInt(strings[i+1]);
			}*/

		} catch (Throwable e) {
			// :( It's ok we have defaults
		}
		getScoreToDb();
	}

	public static void save () {
		try {
			FileHandle filehandle = Gdx.files.external(file);
			
			filehandle.writeString(Boolean.toString(soundEnabled)+"\n", false);
			/*for (int i = 0; i < 5; i++) {
				filehandle.writeString(Integer.toString(highscores[i])+"\n", true);
			}*/
		} catch (Throwable e) {
		}
	}

	/*public static void addScore (int score) {
		for (int i = 0; i < 5; i++) {
			if (highscores[i] < score) {
				for (int j = 4; j > i; j--)
					highscores[j] = highscores[j - 1];
				highscores[i] = score;
				break;
			}
		}
	}*/

	public static void setScoreToDb(int score) {
		//Json object
		Record record = new Record(playerName, score);
		Json json = new Json(JsonWriter.OutputType.json);
		System.out.println(json.toJson(record));

		//HTTP Request
		Net.HttpRequest httpRequest = new Net.HttpRequest(Net.HttpMethods.POST);
		httpRequest.setUrl(URL+URL_POST);
		httpRequest.setHeader(HttpRequestHeader.ContentType, "application/json");
		httpRequest.setContent(json.toJson(record));
		httpRequest.setTimeOut(3000);
		Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
			@Override
			public void handleHttpResponse(Net.HttpResponse httpResponse) {
				internetError = false;
				System.out.println(httpResponse.getResultAsString());
			}

			@Override
			public void failed(Throwable t) {
				System.out.println(t);
				System.out.println("HTTP response failed");
				internetError = true;
			}

			@Override
			public void cancelled() {
				System.out.println("HTTP response cancelled");
			}
		});
		getScoreToDb();
	}

	public static void getScoreToDb() {
		Net.HttpRequest httpRequest = new Net.HttpRequest(Net.HttpMethods.GET);
		httpRequest.setUrl(URL+URL_GET);
		httpRequest.setTimeOut(0);
		Gdx.net.sendHttpRequest(httpRequest, new Net.HttpResponseListener() {
			@Override
			public void handleHttpResponse(Net.HttpResponse httpResponse) {
				internetError = false;
				String result = httpResponse.getResultAsString();
				System.out.println(result);
				highscoresFromServer(result);
				selectBestHighScores();
			}

			@Override
			public void failed(Throwable t) {
				internetError = true;
				System.out.println(t);
				System.out.println("HTTP response failed");
				highscoresMap.clear();
				for (int i = 0; i < MAX_NUM_RECORDS_TO_SHOW; i++) {
					highscoresMap.put(0, "Default");
				}
			}

			@Override
			public void cancelled() {
				System.out.println("HTTP response cancelled");
			}

			void highscoresFromServer(String result) {
				JsonReader reader = new JsonReader();
				JsonValue value = reader.parse(result);
				int index = 0;
				highscoresMap.clear();
				for (JsonValue entry = value.child(); index < value.size; entry = entry.next) {
					highscoresMap.put(entry.getInt("score"), entry.getString("player"));
					index++;
				}
			}

			void selectBestHighScores() {
				TreeMap<Integer, String> selectHighscores = new TreeMap<Integer, String>(Collections.reverseOrder());
				Iterator iterator = highscoresMap.entrySet().iterator();
				for (int i = 0; i < MAX_NUM_RECORDS_TO_SHOW; i++){
					Map.Entry entry = (Map.Entry)iterator.next();
					selectHighscores.put((Integer) entry.getKey(), (String)entry.getValue());
				}
				highscoresMap = selectHighscores;
			}
		});
	}

	private static class Record {
		public final String app_code = "jump";
		public String player;
		public int score;

		public Record(String _player, int _score) {
			player = _player;
			score = _score;
		}
	}
}

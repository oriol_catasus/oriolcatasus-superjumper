var net = require('net');
var ClientsList = require('./ClientsList.js');

//////////////////
var PORT = 43234;
var verbosity = false;
var resendAll = false;
var ClientsList;

main();

function main() {

	ClientsList = new ClientsList();

	net.createServer(function(sock) {
		sock.id = Math.floor(Math.random() * 100000);
		if (verbosity) console.log('CONNECTION RECEIVED: ' + sock.remoteAddress +':'+ sock.remotePort);
	    sock.on('data', function(data) {
	    	var _self = this;
	    	if (verbosity) console.log('DATA ' + sock.remoteAddress + ': ' + data);
	    	data.toString('utf-8').split('\n').forEach(function(message){
	    		parseData(message, _self);
	    	});
	    });
	    sock.on('close', function(data) {
	    	if (verbosity) console.log('Connection closed for socket ' + sock.id);
	    	ClientsList.removeSocket(sock.id);
	    });
	    sock.on('error', function (err) {
		    console.error(err.stack);
		    console.log("Socket error " + sock.id);
		    ClientsList.removeSocket(sock.id);
		});
	    ClientsList.addClient(null, sock, sock.id);
	}).listen(PORT);
	console.log('Server listening on port '+ PORT);

	var consoleInput = function() {
		input( ">> ", function(data) {
			var parts = data.trim().split(' ');
			if (parts[0]) parts[0] = parts[0].toUpperCase();

			switch (true) {
				case (parts[0]=="Q"):
					process.exit(0);
					break;
				case (parts[0]=="LST"):
					var channels = ClientsList.getChannels();
					for (var i in channels) {
						console.log("CHANNELS: " + channels[i]);
						var data = ClientsList.getPlayersDetail(channels[i]);						
						for (var x in data) {
							var detail = data[x];
							var socket = ClientsList.getSocket(detail.id_socket);
							var remoteIP = (socket.socket?socket.socket.remoteAddress:"-");
							var countPlayers = detail.players.length;
							console.log("Socket " + remoteIP + "(" + detail.id_socket + "): " + countPlayers);
						}
					}
					consoleInput();
					break;
				case (parts[0]=="WHEREIS"):
					if (parts[1]) {
						var details = ClientsList.getPlayerDetail(parts[1]);
						console.log ( JSON.stringify(  details ) );
					}
					consoleInput();
					break;
				case (parts[0]=="SEND"):
					if (parts[1]) {
						var socket = ClientsList.getSocket(parts[1]);
						if (socket && socket.socket) {
							socket.socket.write(parts[2] + "\n", function() { console.log("Sent"); });
						}
					}
					setTimeout( consoleInput, 300);
					break;
				case (parts[0]=="SENDALL"):					
					if (parts[1]) {
						ClientsList.sendAll(parts[1]);						
					}
					setTimeout( consoleInput, 300);
					break;
				case (parts[0]=="VERBOSE"):
					if (parts[1] && parts[1].toUpperCase() == "ON") {
						verbosity = true;
					}
					if (parts[1] && parts[1].toUpperCase() == "OFF") {
						verbosity = false;
					}
					consoleInput();
					break;
				default:
					console.log("Incorrect command");
					consoleInput();
					break;
			}

		});
	};

	console.log("----------------------------")
	console.log("LST - List regsitered CHANNELS and its sockets and stats");
	console.log("WHEREIS SUBSCRIBER - List CHANNELS and sockets of player SUBSCRIBER");
	console.log("VERBOSE ON | OFF - Set the server operations verbosity  ON or OFF (default)");
	console.log("SEND ID_SOCKET MESSAGE - Send MESSAGE to the CHANNEL client identified by ID_SOCKET")
	console.log("SENDALL MESSAGE - Send MESSAGE to all clients")
	console.log("----------------------------")
	console.log("Q - Exit");
	console.log("----------------------------")

	consoleInput();
}

function parseData(data, sock) {

	if (verbosity) console.log(data);

	var parts = data.trim().split(':');
	if (parts[0]) parts[0] = parts[0].toUpperCase();
	console.log("->" + parts[0]);
	switch (true) {
		case parts[0]=="SERVER":
			var player = parts[1];
			var game = parts[2];
			ClientsList.setChannelToClient(sock.id, player, game);
			break;
		case parts[0]=="JOIN":
			var player = parts[1];
			var game = parts[2];
			ClientsList.setChannelToClient(sock.id, player, game);			
			ClientsList.sendAll( data + "\n", function() { console.log("join: DATA RESENT", data); } , { exceptions: [sock.id] } );
			resendAll = true;
			break;		
		default:
			var socket = ClientsList.getSocket(sock.id);
			var channel = "";
			if (socket) channel = socket.channel;
			if (verbosity) console.log('RECIEVED DATA (CHANNEL "' + channel + ' "): ' + data);
			if (resendAll) ClientsList.sendAll( data + "\n", function() { console.log("default: DATA RESENT"); }, { exceptions: [sock.id] } );
			break;
	}
}


function input(question, callback) {
	 var stdin = process.stdin, stdout = process.stdout;

	 stdin.resume();
	 stdout.write(question);

	 stdin.once('data', function(data) {
	   data = data.toString().trim();
	   callback(data);
	 });
}
